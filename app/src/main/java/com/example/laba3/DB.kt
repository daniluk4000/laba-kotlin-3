package com.example.laba3

import android.content.ContentValues
import android.content.Context
import android.content.SharedPreferences
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log
import com.google.gson.annotations.SerializedName
import java.io.Serializable

lateinit var pref: SharedPreferences

data class Answer(
    @SerializedName("question") var question: String = "",
    @SerializedName("user") var user: Number = 0,
    @SerializedName("answer") var answer: String = "",
    @SerializedName("correct") var correct: Boolean = false
) : Serializable

data class User(
    @SerializedName("login") var login: String = "",
    @SerializedName("password") var password: String = "",
    @SerializedName("first_name") var first_name: String = "",
    @SerializedName("last_name") var last_name: String = "",
    @SerializedName("surname") var surname: String = "",
    @SerializedName("birthday") var birthday: String = "",
    @SerializedName("rating") var rating: Number = 0,
    @SerializedName("rating") var answers: ArrayList<Answer> = ArrayList()
) : Serializable

class DB(context: Context, factory: SQLiteDatabase.CursorFactory?) :
    SQLiteOpenHelper(context, DATABASE_NAME, factory, DATABASE_VERSION) {
    companion object {
        private const val DATABASE_NAME = "polytech.laba3.db"
        private const val DATABASE_VERSION = 1
        private const val TABLE_USERS = "users"
        private const val TABLE_ANSWERS = "answers"

        private const val COLUMN_ID = "_id"
    }

    override fun onCreate(db: SQLiteDatabase) {
        db.execSQL(
            "CREATE TABLE $TABLE_USERS (" +
                    "$COLUMN_ID INTEGER PRIMARY KEY," +
                    "login TEXT," +
                    "password TEXT," +
                    "first_name TEXT," +
                    "last_name TEXT DEFAULT ''," +
                    "surname TEXT DEFAULT ''," +
                    "birthday TIMESTAMP DEFAULT NULL" +
                    ")"
        )
        db.execSQL(
            "CREATE TABLE $TABLE_ANSWERS (" +
                    "$COLUMN_ID INTEGER PRIMARY KEY," +
                    "user INTEGER," +
                    "question TEXT," +
                    "answer TEXT," +
                    "correct BOOLEAN" +
                    ")"
        )
    }

    override fun onUpgrade(db: SQLiteDatabase, oldVersion: Int, newVersion: Int) {
        db.execSQL("DROP TABLE IF EXISTS TABLE_$TABLE_USERS")
        db.execSQL("DROP TABLE IF EXISTS TABLE_$TABLE_ANSWERS")
        onCreate(db)
    }

    fun getCurrentUser(context: Context): User {
        pref = context.getSharedPreferences("project", Context.MODE_PRIVATE);
        val id = pref.getInt("LOGGED_IN", -1)
        val db = this.writableDatabase;
        val cursor = db.rawQuery("SELECT login FROM $TABLE_USERS WHERE $COLUMN_ID = $id", null);
        cursor.moveToFirst();
        val login = cursor.getString(0);
        return findUserByLogin(login)
    }

    fun loginUser(context: Context, id: Int) {
        pref = context.getSharedPreferences("project", Context.MODE_PRIVATE);
        val editor = pref.edit();
        editor.putInt("LOGGED_IN", id);
        editor.apply();
    }

    fun logoutUser(context: Context) {
        pref = context.getSharedPreferences("project", Context.MODE_PRIVATE);
        val editor = pref.edit();
        editor.clear()
        editor.apply();
    }

    fun isUserLoggedIn(context: Context): Boolean {
        pref = context.getSharedPreferences("project", Context.MODE_PRIVATE);
        return pref.contains("LOGGED_IN")
    }

    fun addUser(user: User): Int {
        val values = ContentValues()
        values.put("login", user.login)
        values.put("password", user.password)
        values.put("first_name", user.first_name)
        values.put("last_name", user.last_name)
        values.put("surname", user.surname)
        values.put("birthday", user.birthday)
        val db = this.writableDatabase;

        val id = db.insert(TABLE_USERS, null, values)
        Log.d("DB", id.toString())
        db.close()
        return id.toInt()
    }

    fun addAnswer(context: Context, answer: AnswerItem, question: String) {
        val values = ContentValues()
        pref = context.getSharedPreferences("project", Context.MODE_PRIVATE);
        val id = pref.getInt("LOGGED_IN", -1)
        values.put("question", question)
        values.put("answer", answer.text)
        values.put("correct", answer.checked.toString())
        values.put("user", id)
        val db = this.writableDatabase;

        db.insert(TABLE_ANSWERS, null, values)
        Log.d("DB", id.toString())
        db.close()
    }

    fun dropTable() {
        val db = this.writableDatabase;
        db.delete(TABLE_USERS, null, null)
        db.delete(TABLE_ANSWERS, null, null)
        db.close()
    }

    fun dropAnswers() {
        val db = this.writableDatabase;
        db.delete(TABLE_ANSWERS, null, null)
        db.close()
    }

    fun getAll(): ArrayList<User> {
        val users = ArrayList<User>()
        val query = "SELECT * FROM $TABLE_USERS"
        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null);
        if (cursor.moveToFirst()) {
            cursor.moveToFirst();
            do {
                val user = this.findUserByLogin(cursor.getString(1))
                users.add(user)
            } while (cursor.moveToNext())
            cursor.close()
        }
        db.close()
        return users;
    }

    fun authUser(login: String, password: String): Int {
        val query =
            "SELECT * FROM $TABLE_USERS WHERE login =  \"$login\" AND password = \"$password\""
        val db = this.writableDatabase;
        val cursor = db.rawQuery(query, null)
        if (cursor.moveToFirst()) {
            val id = cursor.getInt(0)
            cursor.close()
            db.close();
            return id;
        }
        cursor.close()
        db.close();
        return -1;
    }

    fun findUserByLogin(login: String): User {
        val person = User()

        val query =
            "SELECT * FROM $TABLE_USERS WHERE login =  \"$login\""

        val db = this.writableDatabase
        val cursor = db.rawQuery(query, null)

        if (cursor.moveToFirst()) {
            cursor.moveToFirst()
            val answers = ArrayList<Answer>()
            val answer_cursor = db.rawQuery(
                "SELECT * FROM $TABLE_ANSWERS WHERE user = ${cursor.getString(0)}",
                null
            )
            if (answer_cursor.moveToFirst()) {
                answer_cursor.moveToFirst();
                do {
                    val answer = Answer();
                    answer.answer = answer_cursor.getString(2);
                    answer.question = answer_cursor.getString(3);
                    answer.correct = answer_cursor.getString(4) == "true"
                    answers.add(answer)
                } while (answer_cursor.moveToNext())
                answer_cursor.close()
            }
            person.login = cursor.getString(1)
            person.first_name = cursor.getString(3)
            person.last_name = cursor.getString(4)
            person.surname = cursor.getString(5)
            var correct = 0;
            if (answers.size > 0) {
                answers.forEach {
                    if (it.correct)
                        correct++;
                }
                person.rating = correct * 100 / answers.size;
            }
            person.answers = answers
            cursor.close()
        }
        db.close()
        return person
    }

}