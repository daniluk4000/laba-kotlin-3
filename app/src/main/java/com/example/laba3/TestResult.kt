package com.example.laba3

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*
import kotlinx.android.synthetic.main.activity_results.*

class TestResult : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_results)
        val db = DB(this, null);
        val user = db.getCurrentUser(this);
        if (user.answers.isEmpty()) {
            val intent = Intent(this, Home::class.java);
            startActivity(intent);
            finish()
        } else {
            BackHomepage.setOnClickListener {
                val intent = Intent(this, Home::class.java);
                startActivity(intent);
            }
            if (user.rating.toInt() >= 71)
                Status.text = "Тест пройден"
            else
                Status.text = "Тест не пройден"

            ResultPercent.text = "Итоговый результат: ${user.rating}%"
        }
    }
}