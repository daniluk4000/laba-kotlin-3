package com.example.laba3

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_rating.*

class Rating : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_rating)
        val db = DB(this, null);
        val users = db.getAll()
        Users.adapter = UserAdapter(this, users)
        Users.setOnItemClickListener { _, _, position, _ ->
            val intent = Intent(this, UserSingle::class.java);
            intent.putExtra("user", users[position])
            startActivity(intent);
        }
    }
}