package com.example.laba3

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class AnswerItem(
    @SerializedName("text") var text: String = "",
    @SerializedName("checked") var checked: Boolean = false
) : Serializable

data class Question(
    @SerializedName("question") var question: String = "",
    @SerializedName("answers") var answers: ArrayList<AnswerItem> = ArrayList(),
    @SerializedName("correct") var correct: Number = 0
) : Serializable

abstract class BaseActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.header_menu, menu)
        val db = DB(this, null);
        val user = db.getCurrentUser(this);
        if (user.answers.isEmpty())
            menu.findItem(R.id.menu_again).isVisible = false;
        return super.onCreateOptionsMenu(menu)
    }

    fun getTest(): ArrayList<Question> {
        val list = ArrayList<Question>()

        var question = Question()
        var answer = AnswerItem();
        question.question = "Лучший в мире язык программирования"
        question.answers = ArrayList()
        answer = AnswerItem()
        answer.text = "Java"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Kotlin"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "PHP"
        question.answers.add(answer);
        question.correct = 1
        list.add(question)

        question = Question()
        question.question = "Лучший в мире фреймворк для Javascript"
        question.answers = ArrayList()
        answer = AnswerItem()
        answer.text = "React"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Angular"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Vue"
        question.answers.add(answer);
        question.correct = 2
        list.add(question)


        question = Question()
        question.question = "Популярнейший язык программирования среди новичков"
        question.answers = ArrayList()
        answer = AnswerItem()
        answer.text = "HTML"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "CSS"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Pascal"
        question.answers.add(answer);
        question.correct = 2
        list.add(question)

        question = Question()
        question.question = "Лучшая функция дебага в JavaScript"
        question.answers = ArrayList()
        answer = AnswerItem()
        answer.text = "alert"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "console.error"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "console.log"
        question.answers.add(answer);
        question.correct = 1
        list.add(question)

        question = Question()
        question.question = "Лучший учитель"
        question.answers = ArrayList()
        answer = AnswerItem()
        answer.text = "Максим Владимирович Рубцов"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Иван Михайлович Чикунов"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Жизнь"
        question.answers.add(answer);
        question.correct = 2
        list.add(question)

        question = Question()
        question.question = "Лучший вуз"
        question.answers = ArrayList()
        answer = AnswerItem()
        answer.text = "Московский Политехнический Университет"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Корпус на ПК"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Корпус на Автозаводской"
        question.answers.add(answer);
        question.correct = 0
        list.add(question)

        question = Question()
        question.question = "Какова участь программиста"
        question.answers = ArrayList()
        answer = AnswerItem()
        answer.text = "Страдать"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Дебажить всю жизнь"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Смотреть на ошибки компилятора"
        question.answers.add(answer);
        answer = AnswerItem()
        answer.text = "Всё вместе"
        question.answers.add(answer);
        question.correct = 3
        list.add(question)

        return list;
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        pref = getSharedPreferences("project", Context.MODE_PRIVATE)
        when (item.itemId) {
            R.id.menu_again -> {
                val db = DB(this, null)
                db.dropAnswers()
                val intent = Intent(this@BaseActivity, Test::class.java)
                startActivity(intent)
                return true
            }
            R.id.menu_about -> {
                val intent = Intent(this@BaseActivity, About::class.java)
                startActivity(intent)
                return true
            }
            R.id.menu_exit -> {
                val db = DB(this, null);
                db.logoutUser(this);
                val intent = Intent(this@BaseActivity, MainActivity::class.java)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

}