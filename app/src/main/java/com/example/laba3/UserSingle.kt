package com.example.laba3

import android.os.Bundle
import kotlinx.android.synthetic.main.rating_user.*

class UserSingle : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.rating_user)
        val db = DB(this, null);
        val user = intent.getSerializableExtra("user") as User
        Answers.adapter = UserAnswerAdapter(this, user.answers)

        Title.text = "Пользователь ${user.login}"
        FirstName.text = "Имя: \n${user.first_name}"
        LastName.text = "Фамилия: \n${user.last_name}"
        SurName.text = "Отчество: \n${user.surname}"
        Rating.text = "Рейтинг: ${user.rating}%"
    }
}