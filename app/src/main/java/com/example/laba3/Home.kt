package com.example.laba3

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_home.*

class Home : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        val db = DB(this, null)
        val user = db.getCurrentUser(this);
        if (user.answers.isNotEmpty()) {
            OpenTest.text = "Просмотреть результаты"
        }
        OpenTest.setOnClickListener() {
            if (user.answers.isNotEmpty()) {
                val intent = Intent(this, TestResult::class.java);
                startActivity(intent);
            } else {
                val intent = Intent(this, Test::class.java);
                startActivity(intent);
                finish();
            }
        }
        OpenRating.setOnClickListener {
            val intent = Intent(this, Rating::class.java);
            startActivity(intent);
        }
    }
}