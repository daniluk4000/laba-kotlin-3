package com.example.laba3

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ListView
import android.widget.TextView

class UserAdapter(private val context: Context, private val dataSource: ArrayList<User>) :
    BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater;


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.rating_short, parent, false)

        /*
        val answers = rowView.findViewById(R.id.Answers) as ListView
        answers.adapter = UserAnswerAdapter(context, getItem(position).answers)
        val userName = rowView.findViewById(R.id.FirstName) as TextView;
         val userLastName = rowView.findViewById(R.id.LastName) as TextView;
         val userSurName = rowView.findViewById(R.id.SurName) as TextView;

         userName.text = getItem(position).first_name
         userLastName.text = getItem(position).last_name
         userSurName.text = getItem(position).surname*/
        val userLogin = rowView.findViewById(R.id.Login) as TextView;
        userLogin.text = getItem(position).login
        val userRating = rowView.findViewById(R.id.Rating) as TextView;
        userRating.text = "Рейтинг: ${getItem(position).rating}%"

        /*val btn = rowView.findViewById(R.id.Btn) as TextView;
        // val chip = rowView.findViewById(R.id.chip) as Chip;
        btn.text = getItem(position).text

        val CheckBox = rowView.findViewById(R.id.CheckBox) as CheckBox
        if (getItem(position).checked) {
            //chip.isChecked = getItem(position).checked;
//            btn.text = String.format(
//                "%1s ✔",
//                getItem(position).text
//            )
            CheckBox.isChecked = true
        }*/

        return rowView
    }

    override fun getItem(position: Int): User {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

}