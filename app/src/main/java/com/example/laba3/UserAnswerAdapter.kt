package com.example.laba3

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView

class UserAnswerAdapter(private val context: Context, private val dataSource: ArrayList<Answer>) :
    BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater;


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.rating_user_question, parent, false)

        val answer = rowView.findViewById(R.id.Answer) as TextView
        val question = rowView.findViewById(R.id.Question) as TextView
        question.text = "Вопрос: ${getItem(position).answer}"
        answer.text = "Ответ: ${getItem(position).question}"
        /*val btn = rowView.findViewById(R.id.Btn) as TextView;
        // val chip = rowView.findViewById(R.id.chip) as Chip;
        btn.text = getItem(position).text

        val CheckBox = rowView.findViewById(R.id.CheckBox) as CheckBox
        if (getItem(position).checked) {
            //chip.isChecked = getItem(position).checked;
//            btn.text = String.format(
//                "%1s ✔",
//                getItem(position).text
//            )
            CheckBox.isChecked = true
        }*/

        return rowView
    }

    override fun getItem(position: Int): Answer {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

}