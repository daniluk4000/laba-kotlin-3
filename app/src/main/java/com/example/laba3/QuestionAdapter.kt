package com.example.laba3

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.CheckBox
import android.widget.RadioButton
import android.widget.TextView
import com.google.android.material.chip.Chip

class QuestionAdapter(private val context: Context, private val dataSource: ArrayList<AnswerItem>) :
    BaseAdapter() {
    private val inflater: LayoutInflater =
        context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater;


    @SuppressLint("ViewHolder")
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val rowView = inflater.inflate(R.layout.question, parent, false)

        val btn = rowView.findViewById(R.id.Btn) as TextView;
        // val chip = rowView.findViewById(R.id.chip) as Chip;
        btn.text = getItem(position).text

        val CheckBox = rowView.findViewById(R.id.CheckBox) as CheckBox
        if (getItem(position).checked) {
            //chip.isChecked = getItem(position).checked;
//            btn.text = String.format(
//                "%1s ✔",
//                getItem(position).text
//            )
            CheckBox.isChecked = true
        }

        return rowView
    }

    override fun getItem(position: Int): AnswerItem {
        return dataSource[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return dataSource.size
    }

}