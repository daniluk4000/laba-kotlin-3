package com.example.laba3

import android.content.Context
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val db = DB(this, null);
        if (db.isUserLoggedIn(this)) {
            val intent = Intent(this, Home::class.java);
            startActivity(intent);
            finish()
        } else {
            LoginButton.setOnClickListener() {
                Error.visibility = View.INVISIBLE;
                val id = db.authUser(Login.text.toString(), Password.text.toString());
                if (id > 0) {
                    db.loginUser(this, id)
                    val intent = Intent(this, Home::class.java);
                    startActivity(intent);
                    finish();
                } else {
                    Error.text = "Неверное имя пользователя или пароль"
                    Error.visibility = View.VISIBLE;
                }
            }
            Drop.setOnClickListener() {
                db.dropTable();
            }
            RegisterButton.setOnClickListener() {
                val intent = Intent(this, Register::class.java);
                startActivity(intent);
                finish();
            }
        }
    }
}
