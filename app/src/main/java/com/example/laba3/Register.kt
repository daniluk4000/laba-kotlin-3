package com.example.laba3

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.example.laba3.Register
import kotlinx.android.synthetic.main.activity_register.*
import kotlin.Error
import android.content.Context.MODE_PRIVATE
import android.content.SharedPreferences


class Register : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)

        Register.setOnClickListener() {
            Error.visibility = View.INVISIBLE;
            val user = User();
            user.login = InputLogin.text.toString();
            user.password = InputPassword.text.toString();
            user.first_name = InputName.text.toString();
            user.last_name = InputLastName.text.toString();
            user.surname = InputSurname.text.toString();
            user.birthday = InputBirthday.text.toString();
            val db = DB(this, null)
            if (user.login == "" || user.password == "" || user.first_name == "") {
                Error.visibility = View.VISIBLE
                Error.text = "Поля Логин, Пароль и Имя являются обязательными."
            } else if (db.findUserByLogin(user.login).first_name != "") {
                Error.visibility = View.VISIBLE
                Error.text = "Пользователь с таким логином уже существует."
            } else {
                val id = db.addUser(user)
                Error.visibility = View.VISIBLE
                Error.text = id.toString()
                db.loginUser(this, id)
                val intent = Intent(this, Home::class.java);
                startActivity(intent);
                finish();
            }
        }
    }
}