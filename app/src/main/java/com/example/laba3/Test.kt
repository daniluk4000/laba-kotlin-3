package com.example.laba3

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_test.*

class Test : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)
        val db = DB(this, null);
        val questions = getTest();
        pref = getSharedPreferences("project", Context.MODE_PRIVATE);
        var cur = 0;
        var question = questions[cur];
        var adapter = QuestionAdapter(this, question.answers)
        QuestionText.text = "${cur + 1}/${questions.size}\n${question.question}"
        Questions.deferNotifyDataSetChanged()
        Questions.adapter = adapter;
        Questions.setOnItemClickListener { _, _, position, _ ->
            questions[cur].answers.forEach() {
                it.checked = false;
            }
            questions[cur].answers[position].checked = true;

            adapter.notifyDataSetChanged()
        }
        Done.setOnClickListener() {
            val items = questions[cur].answers.filter { x -> x.checked }
            val corrent = questions[cur].answers[questions[cur].correct.toInt()]
            var item = AnswerItem()
            if (items.isNotEmpty()) {
                item = items[0]
                item.checked = item.text == corrent.text;
                db.addAnswer(this, item, question.question)
                if (cur == questions.size - 1) {
                    val intent = Intent(this, TestResult::class.java);
                    startActivity(intent);
                    finish();
                } else {
                    cur++;
                    question = questions[cur];
                    adapter = QuestionAdapter(this, question.answers)
                    QuestionText.text = "${cur + 1}/${questions.size}\n${question.question}"
                    Questions.adapter = adapter
                }
            }
        }

    }
}